#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
FULL_LOCAL_DIR=`cd $LOCAL_DIR && pwd`
source $LOCAL_DIR/vars.sh
DIR_NGINX="/opt/nginx"
LOCATION_HTML=${LOCAL_DIR}${DIR_NGINX}/html
LOCATION_CONF=${LOCAL_DIR}${DIR_NGINX}/conf
NGINX_CONF=$LOCATION_CONF/nginx.conf
NGINX_LOG=$LOCAL_DIR/log/nginx


# Grant privileges for the user nagios
chown -R ${NGINX_UID}.${NGINX_GID} $LOCATION_HTML
mkdir -p $NGINX_LOG
chown -R ${NGINX_UID}.${NGINX_GID} $NGINX_LOG

# Edit nginx.conf set "user nginx;"
if ! grep -q "^user nginx;" $NGINX_CONF; then if grep -q "^user " $NGINX_CONF; then sed -i 's/^user .*/user nginx\;/' $NGINX_CONF;  else sed -i -e '1 s/^/user nginx\;\n/;' $NGINX_CONF; fi; fi

# run nginx and forward "opt" and "log" directories from container to host
sudo docker run -d -p 80:80 --name  $INAME --restart always -v $FULL_LOCAL_DIR/opt:/opt -v $FULL_LOCAL_DIR/log:/var/log $CNAME /opt/nginx/sbin/nginx -p /opt/nginx -g 'daemon off;'
#sudo docker run --name  $INAME --rm --privileged -ti -v $FULL_LOCAL_DIR/opt:/opt -v $FULL_LOCAL_DIR/log:/var/log $CNAME /bin/sh

# it's show ip
#sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" $NAME
