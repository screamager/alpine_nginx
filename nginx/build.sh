#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

sudo docker build \
    --build-arg NGINX_UID=$NGINX_UID \
    --build-arg NGINX_GID=$NGINX_GID \
    -t $INAME .

