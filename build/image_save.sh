#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# for not running docker, use save:
sudo docker save $LOCAL_DIR/$INAME | gzip > $LOCAL_DIR/$INAME.tar.gz

# for running or paused docker, use export:
#sudo docker export $LOCAL_DIR/$INAME | gzip > $LOCAL_DIR/$INAME.tar.gz
