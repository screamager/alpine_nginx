#!/bin/sh

LOCAL_DIR=`dirname $0`;
PREFIX="../prefix"
NGINX_VERSION=$1

cd $LOCAL_DIR
tar -zxvf nginx-${NGINX_VERSION}.tar.gz
cd nginx-${NGINX_VERSION}/
echo "################ ./CONFIGURE #################"
./configure \
    --with-http_ssl_module \
    --with-http_gzip_static_module \
    --prefix=$PREFIX \
    --http-log-path=/var/log/nginx/access.log \
    --error-log-path=/var/log/nginx/error.log
echo "################ MAKE ########################"
make
echo "################ MAKE INSTALL ################"
make install
cd ..
rm -R nginx-${NGINX_VERSION}/
echo "################ Create NGINX-BIN.TAR.GZ ################"
cd prefix
tar czvf ../nginx-bin.tar.gz ./
rm -R ../prefix/*
