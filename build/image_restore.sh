#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# load
sudo gunzip -c $LOCAL_DIR/$INAME.tar.gz | docker load
