#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
FULL_LOCAL_DIR=`cd $LOCAL_DIR && pwd`
source $LOCAL_DIR/vars.sh
NGINX_VERSION=$1

# run nginx and forward "src" directory from container to host
sudo docker run --name  $INAME -v $FULL_LOCAL_DIR/src:/src $CNAME /src/build_nginx.sh $NGINX_VERSION
