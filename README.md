# alpine_nginx

Two docker containers. First for build "nginx", second for run "nginx".

How using:

Create docker image with build eviroment for compiling "nginx" and run container for compile.
```
$ git clone https://gitlab.com/screamager/alpine_nginx.git
$ cd alpine_nginx/build
$ ./build.sh
$ cd src
$ wget 'http://nginx.org/download/nginx-< VERSION >.tar.gz
$ cd ..
$ ./run.sh
$ ls src/nginx-bin.tar.gz
```
Create docker image for start "nginx" and run container.

```
$ cd ../nginx
$ ./build.sh
$ cd ../opt/nginx
$ cp ../build/src/nginx-bin.tar.gz ./
$ tar xzf nginx-bin.tar.gz
$ rm nginx-bin.tar.gz
$ cd ../../
$ ./run.sh
```
If you want to edit image name or container name then you should edit file vars.sh